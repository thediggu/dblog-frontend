import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngSanitize from 'angular-sanitize';
import satellizer from 'satellizer';
import common from './common/common';
import appComponent from './app.component';
import Components from './components/components';

angular.module('app', [
  uiRouter,
  ngSanitize,
  satellizer,
  common,
  Components
])
  .config(['$locationProvider', '$authProvider', ($locationProvider, $authProvider) => {
    "ngInject";
    $locationProvider.html5Mode(false).hashPrefix('!');
    
    $authProvider.facebook({
      clientId: '1903454216651864'
    });

    $authProvider.google({
      clientId: '107698156014-2aq2juo59ta6r492dnos17vg2r6a2l1l.apps.googleusercontent.com'
    });
    
  }])

  .component('app', appComponent)