import './common.scss';
import contentEditable from './contenteditable.directive';

let commonModule = angular.module('app.common', [
])
  .directive('contenteditable', contentEditable)
  .name


export default commonModule;