import template from './feedItem.html';
import controller from './feedItem.controller';
import './feedItem.scss';

export default {
  bindings: {
    data: "="
  },
  template,
  controller
}