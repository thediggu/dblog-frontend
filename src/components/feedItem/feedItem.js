import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import ngStorage from 'ngstorage';
import feedComponent from './feedItem.component';

let feedModule = angular.module('feedItem', [
  uiRouter
])
  .component('feedItem', feedComponent)
  .name;

export default feedModule;