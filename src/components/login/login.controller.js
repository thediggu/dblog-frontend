/**
 * Login controller
 */
class loginController {
  /**
   * Initializes an instance of Login controller class
   * @param {any} $state - State object  
   */
  constructor($state, $auth) {
    'ngInject';
    
    /**
     * Logs the user in
     */
    this.login = () => {
      // Check if login details are correct
      if(this.loginDetails.username === "user" && this.loginDetails.password === "password") {
        $state.transitionTo('feed');
      } else {
        console.log("login failed")
      }
    }

    this.authenticate = (provider) => {
      $auth.authenticate(provider)
        .then(response => {
          console.log(response)
        }, failure => {
          console.log(failure)
        })
    };

  }
}

export default loginController;