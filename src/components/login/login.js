import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import ngStorage from 'ngstorage';
import loginComponent from './login.component';

let loginModule = angular.module('login', [
  uiRouter
])
  .config(($stateProvider, $urlRouterProvider) => {
    $urlRouterProvider.otherwise('login');
    $stateProvider
      .state('login', {
        url: '/login',
        component: 'login'
      })
  })
  .component('login', loginComponent)
  .name;

export default loginModule;