import template from './login.html';
import controller from './login.controller';
import './login.scss';
import './../../../node_modules/bootstrap-social/bootstrap-social.css';

let loginComponent = {
  bindings: {},
  template,
  controller
};

export default loginComponent;