import login from './login/login';
import editor from './editor/editor';
import editorBasic from './editorBasic/editroBasic';
import feed from './feed/feed';
import feedItem from './feedItem/feedItem';

let componentModule = angular.module('app.components', [
  login,
  editor,
  editorBasic,
  feed,
  feedItem
])
  .name;

export default componentModule;