export default [
  {
    title: "Reflection",
    description: "Story of a conversation between two siblings and a mother, with a twist in the end..",
    author: "Swati Tyagi"
  },
  {
    title: "The 'Real' Intelligence",
    description: "Is technology giving life the potential to flourish or destroy itself...?",
    author: "Mangesh Shirke"
  },
  {
    title: "Still can't hate you",
    description: "Story of a family, which gets destroyed due to alcoholism..",
    author: "Pakhi Maurya"
  },
  {
    title: "Govinda Govinda",
    description: "An account of the devotees of Tirupati temple",
    author: "Jyoti Iyer"
  },
  {
    title: "Reflections",
    description: "Story of a conversation between two siblings and a mother, with a twist in the end..",
    author: "Swati Tyagi"
  },
  {
    title: "The 'Fake' Intelligence",
    description: "Is technology giving life the potential to flourish or destroy itself...?",
    author: "Mangesh Shirke"
  },
  {
    title: "Still can hate you",
    description: "Story of a family, which gets destroyed due to alcoholism..",
    author: "Pakhi Maurya"
  },
  {
    title: "Govinda",
    description: "An account of the devotees of Tirupati temple",
    author: "Jyoti Iyer"
  }
]