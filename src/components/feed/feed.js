import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import ngStorage from 'ngstorage';
import feedComponent from './feed.component';

let feedModule = angular.module('feed', [
  uiRouter
])
  .config(($stateProvider, $urlRouterProvider) => {
    $urlRouterProvider.otherwise('login');
    $stateProvider
      .state('feed', {
        url: '/feed',
        component: 'feed'
      })
  })
  .component('feed', feedComponent)
  .name;

export default feedModule;