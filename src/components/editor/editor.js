import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import ngStorage from 'ngstorage';
import editorComponent from './editor.component';

let editorModule = angular.module('editor', [
  uiRouter
])
  .config(($stateProvider, $urlRouterProvider) => {
    $urlRouterProvider.otherwise('login');
    $stateProvider
      .state('editor', {
        url: '/editor',
        component: 'editor'
      })
  })
  .component('editor', editorComponent)
  .name;

export default editorModule;