/**
 * editor page controller
 */
export default class editorController {
  /**
   * Initialises the editor controller instance
   */
  constructor() {

    // Variables and flags
    this.editorContent = "Click here to start typing...";
    this.hasStartedTyping = false;
    this.hideUrlWindow = true;


    // Helper Functions

    /**
     * Converts YouTube link to Embed source url
     * @param {string} url 
     */
    const getYouTubeEmbedUrl = (url) => {
      var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
      var match = url.match(regExp);
  
      if (match && match[2].length == 11) {
        return "//www.youtube.com/embed/" + match[2];
      } else {
        return 'error';
      }
    }

    /**
     * Returns object to be rendered in editor
     * @param {string} type 
     * @param {string} url 
     */
    const renderObject = (type, url) => {
      switch(type) {
      case 'picture':
        return this.editorContent + `<div class="object-container"><img class="image-object" src="${url}" /></div><br>`
      case 'video':

        return this.editorContent + `<div class="object-container"><iframe width="420" height="315" src="${getYouTubeEmbedUrl(url)}" frameborder="0" allowfullscreen></iframe></div><br>`
      case 'audio':
        return this.editorContent + `<div class="object-container"><audio controls src="${url}">Your browser does not support the audio element.</audio></div><br>`
      }
    }

    
    // Editor Functions

    /**
     * Clears the editor for the first time
     */
    this.initClear = () => {
      if(!this.hasStartedTyping) {
        this.editorContent = "";
        this.hasStartedTyping = true;
      }
    }

    /**
     * Opens the URL getter window and assigns content type
     */
    this.openUrlGetterWindow = (urltype = null) => {
      this.contentType = urltype;
      this.hideUrlWindow = false;
    }

    /**
     * Closes the URL getter window
     */
    this.closeUrlGetterWindow = () => {
      this.hideUrlWindow = true;
      this.contentType = null;
      this.newObjectUrl = null;
    }

    /**
     * Inserts object into content
     */
    this.insertObject = () => {
      this.editorContent = renderObject(this.contentType, this.newObjectUrl);
      this.closeUrlGetterWindow();
    }


  }
}