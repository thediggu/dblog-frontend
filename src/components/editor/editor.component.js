import controller from './editor.controller';
import template from './editor.html';
import './editor.scss';

let editorComponent = {
  bindings: {},
  template,
  controller
}

export default editorComponent;