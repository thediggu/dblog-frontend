import template from './editorBasic.html';
import controller from './editorBasic.controller';
import './editorBasic.scss'

export default {
  bindings: {},
  template,
  controller
}