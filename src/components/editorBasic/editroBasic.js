import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import ngStorage from 'ngstorage';
import editorComponent from './editorBasic.component';

let editorModule = angular.module('editorBasic', [
  uiRouter
])
  .config(($stateProvider, $urlRouterProvider) => {
    $urlRouterProvider.otherwise('login');
    $stateProvider
      .state('editorBasic', {
        url: '/editorBasic',
        component: 'editorBasic'
      })
  })
  .component('editorBasic', editorComponent)
  .name;

export default editorModule;