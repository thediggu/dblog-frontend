# DBlog - A simple blogging application

### Developer Docs

* `npm install` will install all dependencies required for the project
* `npm run serve` starts the development server. Make sure you have all dependencies installed before running this command.
* `npm run build` build the application for deployment into the `dist` folder. Copy the contents of that folder and run it via a server to run the app.

