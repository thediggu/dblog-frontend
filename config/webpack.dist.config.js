const path    = require('path');
let config  = require('../webpack.config');

config.output = {
  filename: '[name].bundle.js',
  publicPath: '',
  path: path.resolve(__dirname, '..', 'dist')
};

module.exports = config;